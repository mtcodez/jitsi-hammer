import json
import numpy as np
import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets, linear_model
from sklearn import neural_network
from sklearn.svm import SVR
import subprocess


def download(host, databse, length, tag, output):
	filename = output + '/' + tag
	with open(filename, 'wb') as handle:
	    command = 'curl -s -k -G "http://' + host + '/query" --data-urlencode "db=' + databse + '" --data-urlencode "q=select value from /.*/ where time >= now() - ' + length + "m AND jvb = \'" + tag + "'\""
	    print command
	    process = subprocess.Popen(command, shell=True, stdout=handle)
	    process.wait()
	return filename

# parse json
def parseInfluxDbData(influxdbFile):
	# read to python objects
	with open(influxdbFile) as json_data:
	    d= json.load(json_data)

	series = d['results'][0]['series']

	metrics = []
	sizes = []
	# remove unused columns
	for s in series:
		if s['name'] == 'graceful_shutdown' or s['name'] == 'current_timestamp' or s['name'] == 'avg_upload_jitter' or s['name'] == 'avg_download_jitter':
			continue
		metrics.append(s['name'])
		sizes.append(len(s['values']))

	# allocate lists for later usage
	processed = [[] for x in  xrange(max(sizes))] # [[], [], [], ...]
	firstMaxItem = sizes.index(max(sizes))

	print metrics, sizes
	# append timestamp
	# for f in xrange(0, max(sizes)):
	# 	processed[f].append(series[firstMaxItem]['values'][f][0])

	# append metric value
	# for x in xrange(0, max(sizes)):
	# 	for s in xrange(0, len(series)):
	# 		if series[s]['name'] == 'graceful_shutdown' or series[s]['name'] == 'current_timestamp':
	# 			continue
	# 		if x < len(series[s]['values']) and processed[x][0] == series[s]['values'][x][0]:
	# 			processed[x].append(series[s]['values'][x][1])
	# 		else:
	# 			processed[x].append(None)
	# print 'processed:', processed
	for x in xrange(0, max(sizes)):
		for s in xrange(0, len(series)):
			if series[s]['name'] == 'graceful_shutdown' or series[s]['name'] == 'current_timestamp' or series[s]['name'] == 'avg_upload_jitter' or series[s]['name'] == 'avg_download_jitter':
				continue
			processed[x].append(series[s]['values'][x][1])
	return processed, metrics



def averageProcessed(processed, metrics):
	output = []
	begin = 0
	end = 0
	conference = 0
	while(True):
		if end == len(processed)-1:
			break
		old = conference
		conference = processed[end][metrics.index('participants')]
		if old == conference:
			end = end + 1
		else:
			# average then
			block = np.array([processed[x] for x in xrange(begin, end)])
			output.append(np.mean(block, axis=0).tolist())
			begin = end
	return output

def prepareData(avergaedData, metrics):
	leftSide = []
	rightSide = []
	participantsIndex = metrics.index('participants')
	predictIndex = metrics.index('rtp_loss')
	for i in xrange(2, len(avergaedData)):
		diff = avergaedData[i][participantsIndex] - avergaedData[i-1][participantsIndex]
		if diff <= 0 or avergaedData[i][participantsIndex] == 0:
			continue
		# CPU
		leftSide.append(avergaedData[i][predictIndex]) # cpu after change
		before = list(avergaedData[i-1])
		before.append(diff)
		before.append(avergaedData[i][metrics.index('videostreams')] - avergaedData[i-1][metrics.index('videostreams')])
		rightSide.append(before) # everything before change except cpu
	# print leftSide[0], rightSide[0]
	return leftSide, rightSide

def train(x, y):
	xTrain = x[:-20]
	xTest = x[-20:]
	yTrain = y[:-20]
	yTest = y[-20:]

	lm = linear_model.Lasso(alpha = 0.1, max_iter=10000000)
	# lm = linear_model.Ridge(alpha = 0.5)
	# lm = neural_network.MLPRegressor()
	# lm = SVR(kernel='rbf', C=1e3, gamma=0.1)
	lm.fit(xTrain, yTrain)
	print("Coefficients: \n", lm.coef_)
	print("Residual sum of squares: %.2f"
      % np.mean((lm.predict(xTest) - yTest) ** 2))
	# Explained variance score: 1 is perfect prediction
	print("R Squared: %.2f" % lm.score(xTest, yTest))
	# plt.scatter(np.array(x), np.array(y))

if __name__ == '__main__':
	processed, metrics = parseInfluxDbData('jitsi-videobridge02.ip-172-31-42-182.us-west-2.compute.internal')
	output= averageProcessed(processed, metrics)
	# print output
	output = output[1:]
	leftSide, rightSide = prepareData(output, metrics)
	train(rightSide, leftSide)
	# download('54.187.56.227:8086', 'metrics', '20', 'jitsi-videobridge02.ip-172-31-42-182.us-west-2.compute.internal', ".")
