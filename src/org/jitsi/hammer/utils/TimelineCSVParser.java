package org.jitsi.hammer.utils;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mante Luo on 4/17/16.
 */
public class TimelineCSVParser {
    public static List<HammerTimeline> parse(String path) throws IOException {
        List<HammerTimeline> list = new LinkedList<HammerTimeline>();
        Reader in = new FileReader(path);
        final CSVParser parser = new CSVParser(in, CSVFormat.DEFAULT.withHeader());
        try {
            for (CSVRecord record : parser.getRecords()) {
                if (Integer.parseInt(record.get(0)) < 0 ||
                Integer.parseInt(record.get(1)) < 0 ||  Integer.parseInt(record.get(2)) < 0) {
                    continue;
                }
                list.add(new HammerTimeline(Integer.parseInt(record.get(0)),
                        Integer.parseInt(record.get(1)), Integer.parseInt(record.get(2))));
            }
        } finally {
            parser.close();
            in.close();
        }
        return list;
    }
}
