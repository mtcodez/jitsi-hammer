package org.jitsi.hammer.utils;

/**
 * Created by Mante Luo on 4/17/16.
 */
public class HammerTimeline {
    private final int startup; // ms
    private final int numOfParticipants;
    private final int duration; // ms

    public HammerTimeline(int startup, int numOfParticipants, int duration) {
        this.startup = startup;
        this.numOfParticipants = numOfParticipants;
        this.duration = duration;
    }

    public int getStartup() {
        return startup;
    }

    public int getNumOfParticipants() {
        return numOfParticipants;
    }

    public int getDuration() {
        return duration;
    }
}
